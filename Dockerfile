FROM nginx:alpine 

# MAINTAINER Tanush Yadav <cultholmes>

COPY assets /usr/share/nginx/html/assets
COPY css /usr/share/nginx/html/css
COPY index.html /usr/share/nginx/html/index.html
# COPY nginx.conf /etc/nginx/nginx.conf
# WORKDIR /website

EXPOSE 80


